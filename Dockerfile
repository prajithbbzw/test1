FROM busybox:1.36.0

ENTRYPOINT [ "ping", "-c", "2" ]
CMD [ "8.8.8.8" ]
